import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import "bootstrap/dist/css/bootstrap.css";
import Posts from "../features/posts/Posts";
// import Card from "../component/Card";

const Cars = () => {
  const [data, setData] = useState([]);

  const fetchData = () => {
    fetch(
      "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json"
    )
      .then((res) => res.json())
      .then((result) => {
        setData(result);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="App">
      <Header></Header>

      <div id="load-btn"></div>
      <div id="clear-btn"></div>
      <div id="cars-container" style={{ marginTop: "2%" }}></div>
      <div className="grid-container" id="cars-container"></div>
      <Posts></Posts>
    </div>
  );
};

export default Cars;
