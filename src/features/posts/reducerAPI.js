// A mock function to mimic making an async request for data
import axios from "axios";

export function fetchCount(amount = 1) {
  return new Promise((resolve) =>
    setTimeout(() => resolve({ data: amount }), 500)
  );
}

export const getPostsAPI = async () => {
  const res = await axios.get(
    `https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json`
  );
  return res;
};
