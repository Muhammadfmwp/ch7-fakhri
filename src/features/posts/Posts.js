import React, { useState, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "react-bootstrap";
import { getPosts, data } from "./reducer";
import {
  Grid,
  Card,
  CardContent,
  Container,
  Button,
  Skeleton,
  Typography,
  CardMedia,
} from "@mui/material";
import { useNavigate } from "react-router-dom";

const Posts = () => {
  const posts = useSelector(data);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [filters, setFilter] = useState({
    tipeDriver: "",
    tanggal: "",
    waktuJemput: "",
    jumlahPenumpang: "",
  });
  const [submitFilter, setSubmitFilter] = useState({});

  const fetchData = () => {
    dispatch(getPosts());
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmitFilter(filters);
  };

  const handleChange = (e) => {
    setFilter({ ...filters, [e.target.name]: e.target.value });
  };

//Referensi Ricky Supriyanto FSW-12
  const carFilter = useMemo(() => {
    return posts.filter((car) => {
      const tipe = submitFilter.tipeDriver == "true";
      const date = Date.parse(submitFilter.tanggal);
      const waktu = submitFilter.waktuJemput;
      const person = submitFilter.jumlahPenumpang;
      //ambil data dari car.example.js lalu ubah ke bentuk string
      const dateData = new Date(car.availableAt).getTime();
      const timeData = car.availableAt.split("T");
      const timeData2 = timeData[1].split(":");

      if (
        submitFilter.jumlahPenumpang &&
        submitFilter.tipeDriver &&
        submitFilter.tanggal &&
        submitFilter.waktuJemput
      ) {
        return (
          car.capacity == person &&
          tipe === car.available &&
          date >= dateData &&
          waktu >= timeData2[0]
        );
      }
      return true;
    });
  }, [posts, submitFilter]);
//Referensi Ricky Supriyanto FSW-12

  console.log(posts.status);

  return (
    <Container>
      <Form onSubmit={handleSubmit} onChange={handleChange}>
        <div className="container" style={{ paddingBottom: "20px" }}>
          <div className="col-md-12" style={{ margin: "auto" }}>
            <div
              className="card"
              style={{
                boxShadow: "0px 3px 10px grey",
                marginTop: "-4.5%",
                borderRadius: "8px",
              }}
            >
              <div className="card-body">
                <div className="row">
                  <div className="col-md-2">
                    <p>Tipe Driver</p>
                    <Form.Select
                      name="tipeDriver"
                      id="available"
                      className="form-select mb-2"
                      aria-label="Default select example"
                    >
                      <option value="default">Pilih Tipe Driver</option>
                      <option value="true">Dengan Supir</option>
                      <option value="false">Lepas Kunci</option>
                    </Form.Select>
                  </div>
                  <div className="col-md-2">
                    <p>Tanggal</p>
                    <Form.Control
                      type="date"
                      name="tanggal"
                      id="date"
                      className="form-control mb-2"
                    ></Form.Control>
                  </div>
                  <div className="col-md-3">
                    <p>Waktu Dijemput/Ambil</p>
                    <Form.Control
                      type="time"
                      name="waktuJemput"
                      id="time"
                      className="form-control mb-2"
                    ></Form.Control>
                  </div>
                  <div className="col-md-3">
                    <p>Jumlah Penumpang</p>
                    <Form.Select
                      input=""
                      name="jumlahPenumpang"
                      id="user"
                      className="form-select"
                      aria-label="Default select example"
                    >
                      <option value="default">Jumlah Penumpang</option>
                      <option value="2">2 Penumpang</option>
                      <option value="4">4 Penumpang</option>
                      <option value="6">6 Penumpang</option>
                    </Form.Select>
                  </div>
                  <div className="col-md-2">
                    <button
                      type="submit"
                      className="btn btn-success col-sm-4 text-small btn-green"
                      style={{
                        fontWeight: "bold",
                        color: "white",
                        width: "140px",
                        height: "40px",
                        marginTop: "8%",
                      }}
                      id="filter-btn"
                    >
                      Cari Mobil
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Form>

      <Grid container spacing={3}>
        {carFilter.map((row, i) => (
          <Grid key={i} item xs={4}>
            <Card style={{ height: "auto" }}>
              <CardMedia
                component="img"
                src={row?.image}
                style={{ height: "200px" }}
              />
              <CardContent>
                <b>{row?.manufacture}</b>
              </CardContent>
              <CardContent>Rp. {row?.rentPerDay}</CardContent>
              <CardContent>{row?.description}</CardContent>
              <CardContent>{row?.capacity} people</CardContent>
              <CardContent>{row?.transmission}</CardContent>
              <Button
                className="btn btn-success text-small btn-green"
                style={{
                  fontWeight: "bold",
                  color: "white",
                  width: "350px",
                  height: "50px",
                  backgroundColor: "#5CB85F",
                }}
              >
                Pilih Mobil
              </Button>
            </Card>
          </Grid>
        ))}
        {posts.status === "loading" && <Typography>Loading</Typography>}
      </Grid>
    </Container>
  );
};

export default Posts;
