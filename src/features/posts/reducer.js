import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getPostsAPI } from "./reducerAPI";

const initialState = {
  show: {},
  data: [],
  isLoading: false,
  error: "",
  success: "",
  status: "idle",
};

export const getPosts = createAsyncThunk("posts/data", async () => {
  const response = await getPostsAPI();
  return response.data;
});

export const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getPosts.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getPosts.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      });
  },
});

export const {} = postsSlice.actions;

export const data = (state) => state.posts.data;

export default postsSlice.reducer;
