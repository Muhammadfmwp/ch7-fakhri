import React from "react";
import logo from "./logo.svg";
import Index from "./pages/Index";
import Cars from "./pages/Cars";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Posts from "./features/posts/Posts";
import "./App.css";
import "./css/style.css";
import "./css/all.min.css";
import { Routes, Route } from "react-router-dom";
import { Container } from "@mui/system";

function App() {
  const menus = [
    { path: "/", component: <Index /> },
    { path: "/cars", component: <Cars /> },
  ];
  return (
    <div className="App">
      <Navbar></Navbar>
      <Container style={{ marginTop: "25px", minHeight: "80vh" }}>
        <Routes>
          {menus.map((row, i) => (
            <Route
              key={i}
              exact
              path={row.path}
              element={row.component}
            ></Route>
          ))}
        </Routes>
        <Footer></Footer>
      </Container>
    </div>
  );
}

export default App;
