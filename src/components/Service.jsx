import React from "react";
import { AppBar, Toolbar } from "@mui/material";
import img_service from "../assets/img_service.png";

const Service = () => {
  return (
    <div className="service-section container">
      <a name="service"></a>
      <div className="row">
        <div className="col-sm-6">
          <div className="card card-invis pt-5 pl-5">
            <div className="pl-5">
              <img
                src={img_service}
                alt="Service Car"
                className="img-service pl-5"
              ></img>
            </div>
          </div>
        </div>
        <div className="col-sm-5">
          <div className="card card-invis pt-sm-5 pl-5">
            <h4 className="pt-4 font-weight-bold" style={{ color: "black" }}>
              <b>Best Car Rental for any kind of trip in Bogor!</b>
            </h4>
            <p className="font-weight-light text-small">
              Sewa mobil di Bogor bersama Binar Car Rental jaminan harga lebih
              murah dibandingkan yang lain, kondisi moil baru, serta kualitas
              pelayanan terbaik untuk perjalanan wisata, bisnis, meeting, dll.
            </p>
            <div className="check-list-section">
              <div
                className="icon-circle-small content-center"
                style={{ backgroundColor: "#CFD4ED" }}
              >
                <i
                  className="fa fa-solid fa-check"
                  style={{ color: "#0D28A6" }}
                ></i>
              </div>
              <p
                className="font-weight-light text-small pl-sm-3"
                style={{ marginTop: "2px", marginLeft: "1rem" }}
              >
                Sewa Mobil Dengan Supir di Bogor 12 Jam
              </p>
            </div>
            <div className="check-list-section">
              <div
                className="icon-circle-small content-center"
                style={{ backgroundColor: "#CFD4ED" }}
              >
                <i
                  className="fa fa-solid fa-check"
                  style={{ color: "#0D28A6" }}
                ></i>
              </div>
              <p
                className="font-weight-light text-small pl-sm-3"
                style={{ marginTop: "2px", marginLeft: "1rem" }}
              >
                Sewa Mobil Lepas Kunci di Bogor 24 Jam
              </p>
            </div>
            <div className="check-list-section">
              <div
                className="icon-circle-small content-center"
                style={{ backgroundColor: "#CFD4ED" }}
              >
                <i
                  className="fa fa-solid fa-check"
                  style={{ color: "#0D28A6" }}
                ></i>
              </div>
              <p
                className="font-weight-light text-small pl-sm-3"
                style={{ marginTop: "2px", marginLeft: "1rem" }}
              >
                Sewa Mobil Jangka PAnjang Bulanan
              </p>
            </div>
            <div className="check-list-section">
              <div
                className="icon-circle-small content-center"
                style={{ backgroundColor: "#CFD4ED" }}
              >
                <i
                  className="fa fa-solid fa-check"
                  style={{ color: "#0D28A6" }}
                ></i>
              </div>
              <p
                className="font-weight-light text-small pl-sm-3"
                style={{ marginTop: "2px", marginLeft: "1rem" }}
              >
                Gratis Antar-Jemput Mobil di Bandara
              </p>
            </div>
            <div className="check-list-section">
              <div
                className="icon-circle-small content-center"
                style={{ backgroundColor: "#CFD4ED" }}
              >
                <i
                  className="fa fa-solid fa-check"
                  style={{ color: "#0D28A6" }}
                ></i>
              </div>
              <p
                className="font-weight-light text-small pl-sm-3"
                style={{ marginTop: "2px", marginLeft: "1rem" }}
              >
                Layanan Airport Transfer / Drop in Out
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Service;
