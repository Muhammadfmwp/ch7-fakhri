import React from "react";
import { AppBar, Toolbar } from "@mui/material";
import Button from "@mui/material/Button";
import { NavLink, Route } from "react-router-dom";

const Navbar = () => {
  return (
    <nav
      className="navbar navbar-expand-lg navbar-light header-color navbar-sm"
      id="navbar"
    >
      <div className="container header-color">
        <NavLink to="/" style={{ textDecoration: "none" }}>
          <button
            className="logo logo-margin"
            style={{ border: "none" }}
          ></button>
        </NavLink>
        <div style={{ marginRight: "10%" }}></div>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasNavbar"
          aria-controls="offcanvasNavbar"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className="offcanvas offcanvas-end"
          tabIndex="-1"
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
        >
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
              BCR
            </h5>
            <button
              type="button"
              className="btn-close text-reset"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>

          <div className="offcanvas-body">
            <ul className="navigasi navbar-nav justify-content-end flex-grow-1 pe-5">
              <li className="nav-item me-2">
                <a
                  className="nav-link"
                  style={{ color: "black" }}
                  href="#service"
                >
                  Our Services
                </a>
              </li>
              <li className="nav-item me-2">
                <a
                  className="nav-link"
                  style={{ color: "black" }}
                  href="#whyus"
                >
                  Why Us
                </a>
              </li>
              <li className="nav-item me-2">
                <a
                  className="nav-link"
                  style={{ color: "black" }}
                  href="#testimonial"
                >
                  Testimonial
                </a>
              </li>
              <li className="nav-item me-2 hovernav">
                <a className="nav-link" style={{ color: "black" }} href="#faq">
                  FAQ
                </a>
              </li>
              <li className="nav-item me-2">
                <button type="button" className="btn btn-success btn-green">
                  Register
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
