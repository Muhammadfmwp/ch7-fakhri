import React from "react";
import Button from "@mui/material/Button";
import img_car from "../assets/img_car.png";
import { NavLink, Route } from "react-router-dom";

const Header = () => {
  return (
    <div
      className="car-section header-color"
      style={{ width: "100%", backgroundColor: "#f1f3ff" }}
    >
      <div className="pt-sm-2" style={{ backgroundColor: "#f1f3ff" }}>
        <div className="row">
          <div className="col-sm-6">
            <div
              className="card card-invis pt-5 header-color"
              style={{ backgroundColor: "#f1f3ff" }}
            >
              <h1 className="pt-4 text-bold car-title">
                <b>Sewa & Rental Mobil Terbaik di kawasan Bogor</b>
              </h1>
              <p className="text-car-section text-small">
                Selamat datang di Binar Car Rental. Kami Menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
              <NavLink to="/cars" style={{ textDecoration: "none" }}>
                <Button
                  className="btn btn-2 btn-success col-sm-4 text-small btn-green"
                  style={{
                    fontWeight: "bold",
                    color: "white",
                    width: "180px",
                    height: "50px",
                    backgroundColor: "#5CB85F",
                  }}
                  // onClick="pindah(`index.example.html`)"
                >
                  Mulai Sewa Mobil
                </Button>
              </NavLink>
            </div>
          </div>
          <div className="col-sm-6">
            <div
              className="card card-invis header-color"
              style={{ backgroundColor: "#f1f3ff" }}
            >
              <img
                src={img_car}
                alt="Mobil Marsedes"
                className="align-items-end  img-car"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
