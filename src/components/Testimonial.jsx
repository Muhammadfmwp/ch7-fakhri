import React from "react";
import Button from "@mui/material/Button";
import img_dee from "../assets/img_dee.png";
import img_john from "../assets/img_john.png";
import { NavLink, Route } from "react-router-dom";

const Footer = () => {
  return (
    <div className="testimonial">
      <div className="container">
        <a name="testimonial"></a>
        <h2 className="text-testimonial text-center">
          <b>Testimonial</b>
        </h2>
        <p className="text-center text-small pt-2">
          Berbagai review positif dari para pelanggan kami
        </p>
      </div>
      <div
        id="carouselExampleInterval"
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div
          className="carousel-inner"
          style={{ justifyContent: "space-evenly" }}
        >
          <div className="carousel-item active" data-bs-interval="10000">
            <div className="container">
              <div className="jumbotron-slide2">
                <div className="col-small2">
                  <img
                    src={img_dee}
                    alt="john doe"
                    className="jumbotron-img"
                  ></img>
                </div>
                <div className="col-small10" style={{ marginLeft: "5%" }}>
                  <div className="jumbotron-stars">
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                  </div>
                  <p
                    className="card-text slider-text"
                    style={{ maxWidth: "400px" }}
                  >
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="slider-text2" style={{ color: "black" }}>
                    John Dee 32, Bromo
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="carousel-item" data-bs-interval="2000">
            <div className="container">
              <div className="jumbotron-slide2" style={{ display: "flex" }}>
                <div className="col-small2">
                  <img
                    src={img_john}
                    alt="john doe"
                    className="jumbotron-img"
                  ></img>
                </div>
                <div className="col-small10" style={{ marginLeft: "5%" }}>
                  <div className="jumbotron-stars">
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                  </div>
                  <p
                    className="card-text slider-text"
                    style={{ maxWidth: "400px" }}
                  >
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="slider-text2" style={{ color: "black" }}>
                    John Dee 32, Bromo
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="carousel-item">
            <div className="container">
              <div className="jumbotron-slide2" style={{ display: "flex" }}>
                <div className="col-small2">
                  <img
                    src={img_dee}
                    alt="john doe"
                    className="jumbotron-img"
                  ></img>
                </div>
                <div className="col-small10" style={{ marginLeft: "5%" }}>
                  <div className="jumbotron-stars">
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                    <i
                      className="fa-solid fa-star"
                      style={{ color: "#F9CC00" }}
                    ></i>
                  </div>
                  <p
                    className="card-text slider-text"
                    style={{ maxWidth: "400px" }}
                  >
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="slider-text2" style={{ color: "black" }}>
                    John Dee 32, Bromo
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <center>
        <div className="container carousel-button">
          <div className="row content-center">
            <Button
              className="btn btn-outline-dark  btn-circle-small text"
              style={{ margin: "0 0.6rem" }}
              type="button"
              data-bs-target="#carouselExampleInterval"
              data-bs-slide="prev"
            >
              <i className="fa fa-solid fa-angle-left"></i>
            </Button>
            <Button
              className="btn btn-success btn-circle-small btn-green"
              style={{ margin: " 0 0.6rem" }}
              type="button"
              data-bs-target="#carouselExampleInterval"
              data-bs-slide="next"
            >
              <i className="fa fa-solid fa-angle-right"></i>
            </Button>
          </div>
        </div>
      </center>

      <div className="container">
        <div className="jumbotron-blue align-content-center">
          <div className="text-center">
            <h1 className="text-light text-bold">
              <b>Sewa Mobil di Bogor Sekarang</b>
            </h1>
            <div
              className="text-light text-small card-text"
              style={{ padding: "1.5rem 0" }}
            >
              <center>
                <p style={{ maxWidth: "500px" }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
                </p>
              </center>
            </div>
            <NavLink to="/cars" style={{ textDecoration: "none" }}>
              <Button
                className="btn btn-success btn-2 text-small btn-green"
                style={{
                  borderRadius: "2px",
                  fontWeight: "bold",
                  color: "white",
                  backgroundColor: "#5CB85F",
                }}
              >
                Mulai Sewa Mobil
              </Button>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
