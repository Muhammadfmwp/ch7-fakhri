import React from "react";
// import { AppBar, Toolbar } from "@mui/material";

const Whyus = () => {
  return (
    <div className="why-us-section">
      <div className="container pt-4">
        <a name="whyus"></a>
        <h3 className="text-bold why-us-title">
          <b>Why Us?</b>
        </h3>
        <p className="text-small why-us-text" style={{ marginTop: "20px" }}>
          Mengapa harus pilih Binar Car Rental?
        </p>

        <div className="row d-flex align-content-center content-center">
          <div className="col-sm-3 mb-3">
            <div
              className="card"
              style={{ width: "16rem", borderRadius: "0.4rem" }}
            >
              <div className="card-body">
                <div
                  className="btn-circle-small mt-2 mb-4 content-center"
                  style={{ backgroundColor: "#F9CC00" }}
                >
                  <i className="fa fa-solid fa-thumbs-up outline-icon"></i>
                </div>
                <h6 className="card-title">
                  <b>Mobil Lengkap</b>
                </h6>
                <p className="card-text text-small">
                  Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                  terawat
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-3 mb-3">
            <div
              className="card"
              style={{ width: "16rem", borderRadius: "0.4rem" }}
            >
              <div className="card-body">
                <div
                  className="btn-circle-small mt-2 mb-4 content-center"
                  style={{ backgroundColor: "#FA2C5A" }}
                >
                  <i className="fa fa-solid fa-tag outline-icon"></i>
                </div>
                <h6 className="card-title">
                  <b>Harga Murah</b>
                </h6>
                <p className="card-text text-small">
                  Harga murah dan bersaing, bisa bandingkan harga kami dengan
                  rental mobil lain
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-3 mb-3">
            <div
              className="card"
              style={{ width: "16rem", borderRadius: "0.4rem" }}
            >
              <div className="card-body">
                <div
                  className="btn-circle-small mt-2 mb-4 content-center"
                  style={{ backgroundColor: "#0D28A6" }}
                >
                  <i
                    className="fa fa-solid fa-clock-four outline-icon"
                    style={{ marginLeft: "1px" }}
                  ></i>
                </div>
                <h6 className="card-title">
                  <b>Mobil Lengkap</b>
                </h6>
                <p className="card-text text-small">
                  Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                  terawat
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-3 mb-3">
            <div
              className="card"
              style={{ width: "16rem", borderRadius: "0.4rem" }}
            >
              <div className="card-body">
                <div
                  className="btn-circle-small mt-2 mb-4 content-center"
                  style={{ backgroundColor: "#5CB85F" }}
                >
                  <i className="fa fa-solid fa-award outline-icon"></i>
                </div>
                <h6 className="card-title">
                  <b>Sopir Profesional</b>
                </h6>
                <p className="card-text text-small">
                  Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                  tepat waktu
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Whyus;
