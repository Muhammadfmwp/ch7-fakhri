import React from "react";
import Button from "@mui/material/Button";
import img_car from "../assets/img_car.png";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row mb-5 text-small">
          <div className="col-sm-3" style={{ fontWeight: "100" }}>
            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
            <p>binarcarrental@gmail.com</p>
            <p>081-233-334-808</p>
          </div>

          <div className="col-sm-3">
            <div className="row">
              <a href="index.html#service" className="footer-link pb-3">
                Services
              </a>
            </div>
            <div className="row">
              <a href="index.html#whyus" className="footer-link pb-3">
                Why Us
              </a>
            </div>
            <div className="row">
              <a href="index.html#testimonial" className="footer-link pb-3">
                Testimonial
              </a>
            </div>
            <div className="row">
              <a href="index.html#faq" className="footer-link pb-3">
                FAQ
              </a>
            </div>
          </div>
          <div className="col-sm-3">
            <p>Connect with us</p>
            <div
              className="row footer-center footer-mb"
              style={{ display: "flex" }}
            >
              <div
                className="btn btn-circle-small"
                style={{ backgroundColor: "#0D28A6", marginRight: "12px" }}
              >
                <i className="fa fa-brands fa-facebook-f outline-icon"></i>
              </div>
              <div
                className="btn btn-circle-small"
                style={{ backgroundColor: "#0D28A6", marginRight: "12px" }}
              >
                <i
                  className="fa fa-brands fa-instagram"
                  style={{ color: "white" }}
                ></i>
              </div>
              <div
                className="btn btn-circle-small"
                style={{ backgroundColor: "#0D28A6", marginRight: "12px" }}
              >
                <i className="fa fa-brands fa-twitter outline-icon"></i>
              </div>
              <div
                className="btn btn-circle-small"
                style={{ backgroundColor: "#0D28A6", marginRight: "12px" }}
              >
                <i className="fa fa-thin fa-envelope outline-icon"></i>
              </div>
              <div
                className="btn btn-circle-small"
                style={{ backgroundColor: "#0D28A6", marginRight: "12px" }}
              >
                <i
                  className="fa fa-brands fa-twitch"
                  style={{ color: "white" }}
                ></i>
              </div>
            </div>
          </div>

          <div className="col-sm-2">
            <p>Copyright Binar 2022</p>
            <div className="row footer-center">
              <div className="logo"></div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
