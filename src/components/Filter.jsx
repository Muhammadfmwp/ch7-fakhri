import React from "react";
import Button from "@mui/material/Button";
import img_car from "../assets/img_car.png";

const Footer = () => {
  return (
    <div className="container">
      <div className="col-md-12" style={{ margin: "auto" }}>
        <div
          className="card"
          style={{
            boxShadow: "0px 3px 10px grey",
            marginTop: "-4.5%",
            borderRadius: "8px",
          }}
        >
          <div className="card-body">
            <div className="row">
              <div className="col-md-2">
                <p>Tipe Driver</p>
                <select
                  name="tipeDriver"
                  id="tipeDriver"
                  className="form-select mb-2"
                  aria-label="Default select example"
                >
                  <option value="default">Pilih Tipe Driver</option>
                  <option value="true">Dengan Supir</option>
                  <option value="false">Lepas Kunci</option>
                </select>
              </div>
              <div className="col-md-2">
                <p>Tanggal</p>
                <input
                  type="date"
                  name="Pilih Tanggal"
                  id="date"
                  className="form-control mb-2"
                ></input>
              </div>
              <div className="col-md-3">
                <p>Waktu Dijemput/Ambil</p>
                <input
                  type="time"
                  name="Pilih Tanggal"
                  id="waktuJemput"
                  className="form-control mb-2"
                ></input>
              </div>
              <div className="col-md-3">
                <p>Jumlah Penumpang</p>
                <select
                  input=""
                  name="jumlahPenumpang"
                  id="jumlahPenumpang"
                  className="form-select"
                  aria-label="Default select example"
                >
                  <option value="default">Jumlah Penumpang</option>
                  <option value="2">2 Penumpang</option>
                  <option value="4">4 Penumpang</option>
                  <option value="6">6 Penumpang</option>
                </select>
              </div>
              <div className="col-md-2">
                <button
                  className="btn btn-success col-sm-4 text-small btn-green"
                  style={{
                    fontWeight: "bold",
                    color: "white",
                    width: "140px",
                    height: "40px",
                    marginTop: "8%",
                  }}
                  id="filter-btn"
                >
                  Cari Mobil
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
